from fabric import Connection, task
from invoke import run

production = Connection(
    forward_agent=True, host='elio.gregorgaertner.de', user='justusbeyer'
)


@task
def sync_devdb(c):
    # Sync the database.
    # -- Create a database dump on the server.
    dump_filename = 'my_fabulous_dump.sql'
    path_to_dump_file_on_server = f'/tmp/{dump_filename}'
    production.run(f'pg_dump justusbeyer > {path_to_dump_file_on_server}')
    # -- Copy dump to local machine.
    production.get(path_to_dump_file_on_server)
    # -- Remove dump file from server.
    production.run(f'rm {path_to_dump_file_on_server}')
    # -- Replace local database with dump from production server.
    run('dropdb justusbeyer --if-exists')
    run('createdb --owner=justusbeyer justusbeyer')
    run(f'psql justusbeyer < {dump_filename}')
    # -- Remove local dump file.
    run(f'rm {dump_filename}')

    # Sync media files.
    # -- Clear local media folder.
    run('rm -rf public/media/')
    # -- Copy remote media folder to local project.
    run('scp -r justusbeyer@elio.gregorgaertner.de:justusbeyer/public/media/ public/')
