from django.apps import AppConfig


class RecordingsConfig(AppConfig):
    name = 'apps.recordings'
    label = 'recordings'
    verbose_name = "Aufnahmen"
