from django.views.generic import TemplateView

from .models import AudioRecording, Category, VideoRecording


class RecordingsView(TemplateView):
    category_id = None
    template_name = 'recordings/recordings_page.html'

    def get_context_data(self, *args, **kwargs):
        context_data = super().get_context_data(*args, **kwargs)
        context_data['title'] = Category.objects.get(id=self.category_id).name
        context_data['audio_recordings'] = (
            AudioRecording.objects.filter(category__id=self.category_id)
            .editorial_order()
            .prefetch_roles()
        )
        return context_data


class RecordingProductionsView(RecordingsView):
    category_id = 'recording'

    def get(self, request, *args, **kwargs):
        initial_tab = request.GET.get('initial-tab', default=None)
        if initial_tab not in ('cd', 'youtube'):
            initial_tab = 'cd'
        context = self.get_context_data(initial_tab=initial_tab, **kwargs)
        return self.render_to_response(context)

    def get_context_data(self, *args, **kwargs):
        context_data = super().get_context_data(*args, **kwargs)
        context_data[
            'video_recordings'
        ] = VideoRecording.objects.editorial_order().prefetch_roles()
        return context_data


class RemasteringProductionsView(RecordingsView):
    category_id = 'remastering'
