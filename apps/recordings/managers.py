from django.db.models import Manager, Prefetch, QuerySet


class RecordingQuerySet(QuerySet):
    def editorial_order(self):
        return self.order_by('editorial_order')

    def prefetch_roles(self):
        from .models import Role

        roles_ordered = Role.objects.order_by('editorial_order')
        return self.prefetch_related(Prefetch('roles', queryset=roles_ordered))


AudioRecordingManager = Manager.from_queryset(RecordingQuerySet)


VideoRecordingManager = Manager.from_queryset(RecordingQuerySet)
