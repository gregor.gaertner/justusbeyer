from django.urls import path
from django.utils.translation import gettext_lazy as _

from .views import RecordingProductionsView, RemasteringProductionsView

app_name = 'recordings'
urlpatterns = [
    path(
        # Translators: URL pattern
        _("recording/"),
        view=RecordingProductionsView.as_view(),
        name='recording',
    ),
    path(
        # Translators: URL pattern
        _("remastering/"),
        view=RemasteringProductionsView.as_view(),
        name='remastering',
    ),
]
