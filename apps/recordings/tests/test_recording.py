from apps.recordings.models import COVER_IMAGE_UPLOAD_DIR, get_cover_image_upload_path


def test__get_cover_image_upload_path():
    # Prepare.
    filename = 'some-filename.ext'
    name, extension = filename.split('.')

    # Invoke the function under test.
    path = get_cover_image_upload_path(None, filename)

    # Assert.
    assert path.startswith(COVER_IMAGE_UPLOAD_DIR)
    assert path.endswith('.' + extension)
    assert name not in path
