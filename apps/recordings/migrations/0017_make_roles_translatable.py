from __future__ import unicode_literals

from django.db import migrations, models
from django.db.models import F


def populate_translation_fields(apps, schema_editor):
    Role = apps.get_model('recordings', 'Role')

    Role.objects.update(name_de=F('name'), name_en=F('name'))


class Migration(migrations.Migration):

    dependencies = [('recordings', '0016_translate_categories')]

    operations = [
        migrations.AddField(
            model_name='role',
            name='name_de',
            field=models.CharField(
                help_text='Z. B. „assistent engineer“ oder „editor“.',
                max_length=50,
                null=True,
                verbose_name='Name',
            ),
        ),
        migrations.AddField(
            model_name='role',
            name='name_en',
            field=models.CharField(
                help_text='Z. B. „assistent engineer“ oder „editor“.',
                max_length=50,
                null=True,
                verbose_name='Name',
            ),
        ),
        migrations.RunPython(
            populate_translation_fields, reverse_code=migrations.RunPython.noop
        ),
    ]
