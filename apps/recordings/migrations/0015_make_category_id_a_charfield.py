import django.db.models.deletion
from django.db import migrations, models
from django.db.models import F


def copy_slug_to_new_id(apps, schema_editor):
    Category = apps.get_model('recordings', 'Category')
    Category.objects.update(new_id=F('slug'))


def populate_new_foreign_key(apps, schema_editor):
    AudioRecording = apps.get_model('recordings', 'AudioRecording')
    Category = apps.get_model('recordings', 'Category')

    for audio_recording in AudioRecording.objects.all():
        audio_recording.new_category = audio_recording.category
        audio_recording.save()


class Migration(migrations.Migration):

    dependencies = [('recordings', '0014_rename_categories')]

    operations = [
        migrations.AlterField(
            model_name='category',
            name='slug',
            field=models.SlugField(unique=True, verbose_name='Slug'),
        ),
        migrations.AddField(
            model_name='category',
            name='new_id',
            field=models.CharField(
                blank=True, editable=False, max_length=16, verbose_name='ID'
            ),
        ),
        migrations.RunPython(
            code=copy_slug_to_new_id, reverse_code=migrations.RunPython.noop
        ),
        migrations.AlterField(
            model_name='category',
            name='new_id',
            field=models.CharField(
                editable=False, max_length=16, unique=True, verbose_name='ID'
            ),
        ),
        migrations.AddField(
            model_name='audiorecording',
            name='new_category',
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.PROTECT,
                related_name='new_audio_recordings',
                related_query_name='new_audio_recording',
                to='recordings.Category',
                to_field='new_id',
                verbose_name='Kategorie',
            ),
        ),
        migrations.RunPython(
            code=populate_new_foreign_key, reverse_code=migrations.RunPython.noop
        ),
        migrations.RemoveField(model_name='audiorecording', name='category'),
        migrations.RenameField(
            model_name='audiorecording', old_name='new_category', new_name='category'
        ),
        migrations.AlterField(
            model_name='audiorecording',
            name='category',
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.PROTECT,
                related_name='audio_recordings',
                related_query_name='audio_recording',
                to='recordings.Category',
                to_field='new_id',
                verbose_name='Kategorie',
            ),
        ),
        migrations.RemoveField(model_name='category', name='id'),
        migrations.AlterField(
            model_name='category',
            name='new_id',
            field=models.CharField(
                editable=False,
                max_length=16,
                primary_key=True,
                serialize=False,
                verbose_name='ID',
            ),
        ),
        migrations.AlterField(
            model_name='audiorecording',
            name='category',
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.PROTECT,
                related_name='audio_recordings',
                related_query_name='audio_recording',
                to='recordings.Category',
                verbose_name='Kategorie',
            ),
        ),
        migrations.RenameField(model_name='category', old_name='new_id', new_name='id'),
    ]
