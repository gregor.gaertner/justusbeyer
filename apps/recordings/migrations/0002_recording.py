# Generated by Django 2.0.7 on 2018-07-22 11:41

from django.db import migrations, models

import apps.recordings.models


class Migration(migrations.Migration):

    dependencies = [('recordings', '0001_role_category_recording')]

    operations = [
        migrations.AlterField(
            model_name='recording',
            name='cover_image',
            field=models.ImageField(
                upload_to=apps.recordings.models.get_cover_image_upload_path,
                verbose_name='CD-Cover',
            ),
        ),
        migrations.AlterField(
            model_name='recording',
            name='roles',
            field=models.ManyToManyField(
                related_name='recordings',
                related_query_name='recording',
                to='recordings.Role',
                verbose_name='Rollen',
            ),
        ),
    ]
