import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [('recordings', '0011_category_editorial_order')]

    operations = [
        migrations.RenameModel(old_name='Recording', new_name='AudioRecording'),
        migrations.AlterModelOptions(
            name='audiorecording',
            options={
                'verbose_name': 'Audioaufnahme',
                'verbose_name_plural': 'Audioaufnahmen',
            },
        ),
        migrations.AlterField(
            model_name='audiorecording',
            name='category',
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.PROTECT,
                related_name='audio_recordings',
                related_query_name='audio_recording',
                to='recordings.Category',
                verbose_name='Kategorie',
            ),
        ),
        migrations.AlterField(
            model_name='audiorecording',
            name='roles',
            field=models.ManyToManyField(
                related_name='audio_recordings',
                related_query_name='audio_recording',
                to='recordings.Role',
                verbose_name='Rollen',
            ),
        ),
        migrations.CreateModel(
            name='VideoRecording',
            fields=[
                (
                    'id',
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name='ID',
                    ),
                ),
                (
                    'editorial_order',
                    models.PositiveSmallIntegerField(
                        default=0, verbose_name='Reihenfolge'
                    ),
                ),
                (
                    'internal_title',
                    models.CharField(
                        help_text='Nur zum internen Gebrauch, wird nicht im Frontend angezeigt.',
                        max_length=255,
                        unique=True,
                        verbose_name='Titel (intern)',
                    ),
                ),
                (
                    'display_title',
                    models.CharField(
                        help_text='Wird im Frontend angezeigt.',
                        max_length=255,
                        unique=True,
                        verbose_name='Titel (öffentlich)',
                    ),
                ),
                (
                    'youtube_id',
                    models.CharField(
                        help_text='Bitte aus der Youtube-URL entnehmen. Die ID wird dort links begrenzt durch „v=“ und rechts ggf. durch das nächste „&“.',
                        max_length=20,
                        unique=True,
                        verbose_name='Youtube-ID',
                    ),
                ),
                (
                    'roles',
                    models.ManyToManyField(
                        related_name='video_recordings',
                        related_query_name='video_recording',
                        to='recordings.Role',
                        verbose_name='Rollen',
                    ),
                ),
            ],
            options={
                'verbose_name': 'Videoaufnahme',
                'verbose_name_plural': 'Videoaufnahmen',
            },
        ),
    ]
