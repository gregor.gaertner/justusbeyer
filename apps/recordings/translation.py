from modeltranslation.translator import TranslationOptions, register

from django.conf import settings

from .models import Award, AwardAttribution, Category, Role


@register(Role)
class RoleTranslationOptions(TranslationOptions):
    fields = ('name',)
    required_languages = settings.ALL_LANGUAGE_CODES


@register(Category)
class CategoryTranslationOptions(TranslationOptions):
    fields = ('name', 'slug')
    required_languages = settings.ALL_LANGUAGE_CODES


@register(Award)
class AwardTranslationOptions(TranslationOptions):
    fields = ('name',)
    required_languages = settings.ALL_LANGUAGE_CODES


@register(AwardAttribution)
class AwardAttributionTranslationOptions(TranslationOptions):
    fields = ('category', 'free_text')
