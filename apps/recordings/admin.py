from admin_ordering.admin import OrderableAdmin
from image_cropping import ImageCroppingMixin
from modeltranslation.admin import TranslationAdmin, TranslationStackedInline

from django.contrib import admin
from django.utils.html import format_html

from .models import (
    AudioRecording,
    Award,
    AwardAttribution,
    Category,
    Role,
    VideoRecording,
)


@admin.register(Role)
class RoleAdmin(OrderableAdmin, TranslationAdmin):
    fields = ('name',)
    list_display = ('name', 'editorial_order')
    list_editable = ('editorial_order',)
    list_display_links = ('name',)
    ordering = ('editorial_order',)
    search_fields = ('name',)

    ordering_field = 'editorial_order'
    ordering_field_hide_input = True


@admin.register(Category)
class CategoryAdmin(OrderableAdmin, TranslationAdmin):
    fields = ('name', 'slug')
    list_display = ('name', 'slug', 'editorial_order')
    list_editable = ('editorial_order',)
    list_display_links = ('name',)
    ordering = ('editorial_order',)
    readonly_fields = ('name', 'slug')
    search_fields = ('name',)

    ordering_field = 'editorial_order'
    ordering_field_hide_input = True


@admin.register(Award)
class AwardAdmin(TranslationAdmin):
    fields = ('name', 'logo')
    list_display = ('name',)
    ordering = ('name',)
    search_fields = ('name',)


class AwardAttributionInline(OrderableAdmin, TranslationStackedInline):
    model = AwardAttribution
    fields = ('award', 'attribution_type', 'category', 'free_text')
    ordering = ('attribution_type', 'award', 'category')


@admin.register(AudioRecording)
class AudioRecordingAdmin(ImageCroppingMixin, OrderableAdmin, admin.ModelAdmin):
    fieldsets = (
        (None, {'fields': ('title', 'category', 'roles', 'commercial_url')}),
        (
            "Cover-Abbildung",
            {
                'fields': (
                    'cover_image',
                    'cover_format',
                    'crop_jewelcase_booklet',
                    'crop_digipak_front',
                    'crop_keepcase_front',
                    'use_original_image',
                )
            },
        ),
    )
    inlines = (AwardAttributionInline,)
    list_display = ('title', 'cover_image_thumbnail', 'category', 'editorial_order')
    list_editable = ('editorial_order',)
    list_display_links = ('title',)
    list_filter = ('roles', 'category', 'use_original_image')
    ordering = ('editorial_order',)
    search_fields = ('title',)

    ordering_field = 'editorial_order'
    ordering_field_hide_input = True

    def cover_image_thumbnail(self, obj):
        return format_html(
            '<img src="{url}" style="height: 75px;" />', url=obj.cover_image.url
        )

    cover_image_thumbnail.short_description = "Cover-Bild"


@admin.register(VideoRecording)
class VideoRecordingAdmin(OrderableAdmin, admin.ModelAdmin):
    fields = ('title', 'youtube_id', 'roles')
    list_display = ('title', 'youtube_id', 'editorial_order')
    list_editable = ('editorial_order',)
    list_display_links = ('title',)
    list_filter = ('roles',)
    ordering = ('editorial_order',)
    search_fields = ('title', 'youtube_id')

    ordering_field = 'editorial_order'
    ordering_field_hide_input = True
