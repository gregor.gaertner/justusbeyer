import os
import uuid

from image_cropping import ImageRatioField

from django.db import models

from apps.core.models import EditorialOrderingBase

from .managers import AudioRecordingManager, VideoRecordingManager


class Role(EditorialOrderingBase):
    name = models.CharField(
        verbose_name="Name",
        help_text="z. B. „assistent engineer“ oder „editor“.",
        max_length=50,
    )

    class Meta:
        verbose_name = "Rolle"
        verbose_name_plural = "Rollen"

    def __str__(self):
        return self.name


class Category(EditorialOrderingBase):
    id = models.CharField(
        editable=False, max_length=16, primary_key=True, verbose_name="ID"
    )
    name = models.CharField(max_length=50, verbose_name="Name")
    slug = models.SlugField(unique=True, verbose_name="Slug")

    class Meta:
        verbose_name = "Kategorie"
        verbose_name_plural = "Kategorien"

    def __str__(self):
        return self.name


COVER_IMAGE_UPLOAD_DIR = 'recordings/'


def get_cover_image_upload_path(instance, filename):
    """
    Return a cover image’s upload path with the original filename replaced by a UUID.
    """
    extension = filename.split('.')[-1]
    uuid_filename = f'{uuid.uuid4()}.{extension}'
    return os.path.join(COVER_IMAGE_UPLOAD_DIR, uuid_filename)


class AudioRecording(EditorialOrderingBase):
    title = models.CharField(
        verbose_name="Titel",
        help_text="Nur zum internen Gebrauch, wird nicht im Frontend angezeigt.",
        max_length=255,
        unique=True,
    )
    cover_image = models.ImageField(
        verbose_name="Bilddatei", upload_to=get_cover_image_upload_path
    )
    JEWELCASE_BOOKLET = 'jewelcase-booklet'
    _JEWELCASE_BOOKLET_LABEL = "Jewelcase: Booklet (120 × 120 mm)"
    DIGIPAK_FRONT = 'digipak-front'
    _DIGIPAK_FRONT_LABEL = "Digipak: Frontansicht (139,5 × 125 mm)"
    KEEPCASE_FRONT = 'keepcase-front'
    _KEEPCASE_FRONT_LABEL = "DVD Keepcase: Frontansicht (135 × 190 mm)"
    COVER_FORMAT_CHOICES = (
        (JEWELCASE_BOOKLET, _JEWELCASE_BOOKLET_LABEL),
        (DIGIPAK_FRONT, _DIGIPAK_FRONT_LABEL),
        (KEEPCASE_FRONT, _KEEPCASE_FRONT_LABEL),
    )
    cover_format = models.CharField(
        choices=COVER_FORMAT_CHOICES,
        default=JEWELCASE_BOOKLET,
        help_text="das von der Bilddatei dargestellte Verpackungsformat",
        max_length=20,
        verbose_name="Format",
    )
    _COVER_FORMAT_CROP_FIELD_MAP = {
        JEWELCASE_BOOKLET: 'crop_jewelcase_booklet',
        DIGIPAK_FRONT: 'crop_digipak_front',
        KEEPCASE_FRONT: 'crop_keepcase_front',
    }
    crop_jewelcase_booklet = ImageRatioField(
        help_text=f"wirksam, wenn Format „{_JEWELCASE_BOOKLET_LABEL}“ ausgewählt",
        image_field='cover_image',
        size='400x400',
        verbose_name="Zuschnitt für Jewelcase-Booklet",
    )
    crop_digipak_front = ImageRatioField(
        help_text=f"wirksam, wenn Format „{_DIGIPAK_FRONT_LABEL}“ ausgewählt",
        image_field='cover_image',
        size='425x400',
        verbose_name="Zuschnitt für Digipak-Frontansicht",
    )
    crop_keepcase_front = ImageRatioField(
        help_text=f"wirksam, wenn Format „{_KEEPCASE_FRONT_LABEL}“ ausgewählt",
        image_field='cover_image',
        size='400x563',
        verbose_name="Zuschnitt für Keepcase-Frontansicht",
    )
    use_original_image = models.BooleanField(
        default=False,
        help_text=(
            "@Justus: Bitte den Status dieser Checkbox so lassen, wie du ihn vorfindest. "
            "Danke! Gregor"
        ),
        verbose_name="Hochgeladene Bilddatei unverändert verwenden",
    )
    roles = models.ManyToManyField(
        to=Role,
        verbose_name="Rollen",
        related_name='audio_recordings',
        related_query_name='audio_recording',
    )
    category = models.ForeignKey(
        to=Category,
        null=True,
        on_delete=models.PROTECT,
        related_name='audio_recordings',
        related_query_name='audio_recording',
        verbose_name="Kategorie",
    )
    commercial_url = models.URLField(
        blank=True,
        help_text="Link zu einer Webseite, auf der diese Aufnahme käuflich erworben werden kann",
        verbose_name="Verkaufs-URL",
    )

    objects = AudioRecordingManager()

    class Meta:
        verbose_name = "Audioaufnahme"
        verbose_name_plural = "Audioaufnahmen"

    def __str__(self):
        return self.title

    def get_crop_field(self):
        """
        Return the valid crop field’s field name.

        Which crop field is valid is determined by the `cover_format` field’s value.
        """
        return self._COVER_FORMAT_CROP_FIELD_MAP[self.cover_format]


class VideoRecording(EditorialOrderingBase):
    title = models.CharField(
        verbose_name="Titel",
        help_text="Nur zum internen Gebrauch, wird nicht im Frontend angezeigt.",
        max_length=255,
        unique=True,
    )
    youtube_id = models.CharField(
        verbose_name="Youtube-ID",
        help_text=(
            "Bitte aus der Youtube-URL entnehmen.<br/>"
            "<br/>"
            "<b>Beispiel 1:</b><br/>"
            "<i>In URLs der Domain „youtube.com“ wird die ID links begrenzt durch „v=“ und "
            "rechts ggf. durch das nächste „&“.</i><br/>"
            "URL = https://www.youtube.com/watch?v=8Kdso5-9FEU&feature=youtu.be<br/>"
            "Youtube-ID = 8Kdso5-9FEU<br/>"
            "<br/>"
            "<b>Beispiel 2:</b><br/>"
            "<i>In URLs der Domain „youtu.be“ erscheint die ID hinter dem Schrägstrich.</i>"
            "<br/>"
            "URL = https://youtu.be/T8RRmIQJI68<br/>"
            "Youtube-ID = T8RRmIQJI68"
        ),
        max_length=20,
        unique=True,
    )
    roles = models.ManyToManyField(
        to=Role,
        verbose_name="Rollen",
        related_name='video_recordings',
        related_query_name='video_recording',
    )

    objects = VideoRecordingManager()

    class Meta:
        verbose_name = "Videoaufnahme"
        verbose_name_plural = "Videoaufnahmen"

    def __str__(self):
        return self.title


class Award(models.Model):
    name = models.CharField(
        verbose_name="Name",
        help_text="z. B. „Preis der deutschen Schallplattenkritik“ oder „Opus Klassik“",
        max_length=255,
    )
    logo = models.ImageField(verbose_name="Logo", upload_to='awards/')

    class Meta:
        verbose_name = "Preis"
        verbose_name_plural = "Preise"

    def __str__(self):
        return self.name


class AwardAttribution(models.Model):
    audio_recording = models.ForeignKey(
        to=AudioRecording,
        on_delete=models.CASCADE,
        related_name='award_attributions',
        related_query_name='award_attribution',
        verbose_name="Audioaufnahme",
    )
    award = models.ForeignKey(
        to=Award,
        on_delete=models.PROTECT,
        related_name='award_attributions',
        related_query_name='award_attribution',
        verbose_name="Preis",
    )

    NOMINATED = 'nominated'
    WON = 'won'
    ATTRIBUTION_TYPE_CHOICES = ((NOMINATED, "nominiert"), (WON, "gewonnen"))
    attribution_type = models.CharField(
        choices=ATTRIBUTION_TYPE_CHOICES,
        default=NOMINATED,
        max_length=9,
        verbose_name="Attributionstyp",
    )

    category = models.CharField(blank=True, max_length=255, verbose_name="Kategorie")
    free_text = models.TextField(blank=True, verbose_name="Freitext")

    class Meta:
        verbose_name = "Preisattribution"
        verbose_name_plural = "Preisattributionen"
