from django.db import migrations, models


def write_display_names(apps, schema_editor):
    Artist = apps.get_model('references', 'Artist')
    for artist in Artist.objects.all():
        if artist.artist_name:
            display_name = artist.artist_name
        elif artist.title:
            display_name = f"{artist.title} {artist.first_name} {artist.last_name}"
        else:
            display_name = f"{artist.first_name} {artist.last_name}"
        artist.display_name = display_name
        artist.save()


def write_sort_keys(apps, schema_editor):
    Artist = apps.get_model('references', 'Artist')
    for artist in Artist.objects.all():
        artist._sort_key = artist.artist_name or artist.last_name
        artist.save()


class Migration(migrations.Migration):

    dependencies = [('references', '0002_artist')]

    operations = [
        migrations.AddField(
            model_name='artist',
            name='display_name',
            field=models.CharField(
                default='ARTIST',
                editable=False,
                max_length=562,
                verbose_name='angezeigter Name',
            ),
            preserve_default=False,
        ),
        migrations.RunPython(write_display_names, reverse_code=migrations.RunPython.noop),
        migrations.AddField(
            model_name='artist',
            name='_sort_key',
            field=models.CharField(default='SORTKEY', max_length=255),
            preserve_default=False,
        ),
        migrations.RunPython(write_sort_keys, reverse_code=migrations.RunPython.noop),
    ]
