from django.contrib import admin

from .models import Artist, Corporation


@admin.register(Artist)
class ArtistAdmin(admin.ModelAdmin):
    fields = ('title', 'first_name', 'last_name', 'artist_name', 'display_name')
    list_display = ('title', 'first_name', 'last_name', 'artist_name')
    list_display_links = ('last_name', 'artist_name')
    readonly_fields = ('display_name',)
    search_fields = ('title', 'first_name', 'last_name', 'artist_name', 'display_name')


@admin.register(Corporation)
class CorporationAdmin(admin.ModelAdmin):
    fields = ('name',)
    list_display = ('name',)
    list_display_links = ('name',)
    search_fields = ('name',)
