from django.urls import path
from django.utils.translation import gettext_lazy as _

from .views import ReferencesView

app_name = 'references'
# Translators: URL pattern
urlpatterns = [path(_("references/"), view=ReferencesView.as_view(), name='references')]
