from django.db.models import Manager
from django.db.models.query import QuerySet


class ArtistQuerySet(QuerySet):
    def display_names_list(self):
        return self.order_by('_sort_key').values_list('display_name', flat=True)


class ArtistManager(Manager.from_queryset(ArtistQuerySet)):
    pass


class CorporationQuerySet(QuerySet):
    def names_list(self):
        return self.order_by('name').values_list('name', flat=True)


class CorporationManager(Manager.from_queryset(CorporationQuerySet)):
    pass
