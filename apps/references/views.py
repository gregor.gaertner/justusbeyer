from django.views.generic import TemplateView
from .models import Artist, Corporation


class ReferencesView(TemplateView):
    template_name = 'references/references.html'

    def get_context_data(self, *args, **kwargs):
        return {
            'artist_names': Artist.objects.display_names_list(),
            'corporation_names': Corporation.objects.names_list(),
        }
