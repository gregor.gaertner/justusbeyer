from django.db import models

from .managers import ArtistManager, CorporationManager


class Artist(models.Model):
    title = models.CharField(
        verbose_name="Titel", help_text="z. B. „Prof. h. c.“", max_length=50, blank=True
    )
    first_name = models.CharField(verbose_name="Vorname", max_length=255)
    last_name = models.CharField(verbose_name="Nachname", max_length=255)
    artist_name = models.CharField(
        verbose_name="Künstlername",
        help_text="Wird, wenn angegeben, anstelle des bürgerlichen Namens angezeigt.",
        max_length=255,
        blank=True,
    )
    display_name = models.CharField(
        verbose_name="angezeigter Name", max_length=562, editable=False
    )
    _sort_key = models.CharField(max_length=255)

    objects = ArtistManager()

    class Meta:
        verbose_name = "Musiker_in"
        verbose_name_plural = "Musiker_innen"

    def __str__(self):
        if self.artist_name:
            return self.artist_name
        return f"{self.first_name} {self.last_name}"

    def save(self):
        self.display_name = self._get_display_name()
        self._sort_key = self._get_sort_key()
        super().save()

    def _get_display_name(self):
        if self.artist_name:
            return self.artist_name
        if self.title:
            return f"{self.title} {self.first_name} {self.last_name}"
        return f"{self.first_name} {self.last_name}"

    def _get_sort_key(self):
        return self.artist_name or self.last_name


class Corporation(models.Model):
    name = models.CharField(verbose_name="Name", max_length=255)

    objects = CorporationManager()

    class Meta:
        verbose_name = "Körperschaft"
        verbose_name_plural = "Körperschaften"

    def __str__(self):
        return self.name
