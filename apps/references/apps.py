from django.apps import AppConfig


class ReferencesConfig(AppConfig):
    name = 'apps.references'
    label = 'references'
    verbose_name = "Referenzen"
