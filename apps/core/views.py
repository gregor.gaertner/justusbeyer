from django.views.generic import TemplateView

from apps.recordings.models import AudioRecording, Category, VideoRecording
from apps.references.models import Artist, Corporation


class ContactView(TemplateView):
    template_name = 'core/contact.html'


class ImprintView(TemplateView):
    template_name = 'core/imprint.html'


class IndexView(TemplateView):
    template_name = 'core/index.html'

    def get_context_data(self, *args, **kwargs):
        context_data = super().get_context_data(*args, **kwargs)

        PREVIEW_COUNT__AUDIO = 12
        PREVIEW_COUNT__VIDEO = 6

        # Gather recording productions.
        category = Category.objects.get(id='recording')
        audio_recordings = (
            AudioRecording.objects.filter(category=category)
            .editorial_order()
            .prefetch_roles()
        )
        video_recordings = VideoRecording.objects.editorial_order().prefetch_roles()
        context_data.update(
            {
                'recording': {
                    'title': category.name,
                    'audio_recordings': audio_recordings[:PREVIEW_COUNT__AUDIO],
                    'video_recordings': video_recordings[:PREVIEW_COUNT__VIDEO],
                }
            }
        )

        # Gather references.
        context_data.update(
            {
                'references': {
                    'artist_names': Artist.objects.display_names_list(),
                    'corporation_names': Corporation.objects.names_list(),
                }
            }
        )

        # Gather remastering productions.
        category = Category.objects.get(id='remastering')
        remastering = (
            AudioRecording.objects.filter(category=category)
            .editorial_order()
            .prefetch_roles()
        )
        context_data.update(
            {
                'remastering': {
                    'title': category.name,
                    'recordings': remastering[:PREVIEW_COUNT__AUDIO],
                }
            }
        )

        return context_data
