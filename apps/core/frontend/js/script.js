// -------------------------------------------------------------------------------------------------
// mobile menu

var body = document.querySelector('body');
var hamburger = document.querySelector('.header__hamburger');
var mobileMenu = document.querySelector('.header__mobile-menu');


hamburger.addEventListener('click', function() {
    mobileMenu.classList.toggle('header__mobile-menu--hidden');
    hamburger.classList.toggle('header__hamburger--active');
});


function closeMobileMenu(event) {
    if (event.target.matches('.header__mobile-menu, .header__mobile-menu *')) {
        return;
    }
    if (event.target.matches('.header__hamburger, .header__hamburger *')) {
        return;
    }
    mobileMenu.classList.add('header__mobile-menu--hidden');
    hamburger.classList.remove('header__hamburger--active');
}


body.addEventListener('mousedown', closeMobileMenu);




// -------------------------------------------------------------------------------------------------
// language selector

var languageSelector = document.querySelector('.language-selector');
var currentLanguage = document.querySelector('.language-selector__current-language');

var languageSelectorIsActive = false;


function showAvailableLanguages() {
    languageSelector.classList.add('language-selector--active');
    languageSelectorIsActive = true;
}


function hideAvailableLanguages() {
    languageSelector.classList.remove('language-selector--active');
    languageSelectorIsActive = false;
}


function currentLanguageClicked() {
    if (!(languageSelectorIsActive)) {
        showAvailableLanguages();
    } else {
        hideAvailableLanguages();
    }
}


function bodyClicked(event) {
    if (event.target.matches('.language-selector, .language-selector *')) {
        return;
    }
    hideAvailableLanguages();
}


currentLanguage.addEventListener('mouseenter', showAvailableLanguages);
languageSelector.addEventListener('mouseleave', hideAvailableLanguages);

currentLanguage.addEventListener('mousedown', currentLanguageClicked);
body.addEventListener('mousedown', bodyClicked);

