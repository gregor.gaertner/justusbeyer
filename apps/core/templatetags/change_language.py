from django import template
from django.urls import resolve, reverse
from django.utils import translation

register = template.Library()


@register.simple_tag(takes_context=True)
def change_to_language(context, language_code=None, *args, **kwargs):
    """
    Return the active page’s url in the specified language.

    Usage: {% change_to_language 'en' %}
    """
    url_parts = resolve(context['request'].path)
    with translation.override(language=language_code):
        url = reverse(url_parts.view_name, args=url_parts.args, kwargs=url_parts.kwargs)
    return url
