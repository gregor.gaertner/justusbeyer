from .constants import UI_LABELS
from .site_map import (
    ALL_MENU_ITEMS,
    CONTACT,
    FOOTER_MENU_ITEMS__PRIMARY,
    FOOTER_MENU_ITEMS__SECONDARY,
    HEADER_MENU_ITEMS,
)


def site_map(request):
    """
    Add site map data to the context.
    """
    return {
        'site_map': {item.id: item for item in ALL_MENU_ITEMS},
        'header_menu_items': HEADER_MENU_ITEMS,
        'header_contact': CONTACT,
        'footer_menu_items__primary': FOOTER_MENU_ITEMS__PRIMARY,
        'footer_menu_items__secondary': FOOTER_MENU_ITEMS__SECONDARY,
    }


def ui_labels(request):
    """
    Provide common user interface labels for linguistic uniformity.
    """
    return {'ui_labels': UI_LABELS}
