from django.db import models


class EditorialOrderingBase(models.Model):
    """
    Provide an integer field for editorial ordering.
    """

    editorial_order = models.PositiveSmallIntegerField(
        default=0, verbose_name="Reihenfolge"
    )

    class Meta:
        abstract = True
