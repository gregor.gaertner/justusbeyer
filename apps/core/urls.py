from django.urls import path
from django.utils.translation import gettext_lazy as _

from .views import ContactView, ImprintView, IndexView

app_name = 'core'
urlpatterns = [
    path("", IndexView.as_view(), name='index'),
    # Translators: URL pattern
    path(_("contact/"), view=ContactView.as_view(), name='contact'),
    # Translators: URL pattern
    path(_("imprint/"), view=ImprintView.as_view(), name='imprint'),
]
