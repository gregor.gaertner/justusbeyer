from typing import Optional

import attr

from django.utils.translation import gettext_lazy as _

from apps.recordings.models import Category


@attr.s(frozen=True)
class MenuItem:
    id: str = attr.ib()
    url_name: str = attr.ib()
    label: str = attr.ib()
    anchor_name: Optional[str] = attr.ib(default=None)


recording_category, __ = Category.objects.update_or_create(
    id='recording',
    defaults={
        'name_en': "Recording",
        'name_de': "Aufnahmen",
        'slug_en': "recording",
        'slug_de': "aufnahmen",
    },
)
remastering_category, __ = Category.objects.update_or_create(
    id='remastering',
    defaults={
        'name_en': "Remastering",
        'name_de': "Remastering",
        'slug_en': "remastering",
        'slug_de': "remastering",
    },
)


ABOUT = MenuItem(
    id='about',
    url_name='core:index',
    # Translators: section title
    label=_("About"),
    # Translators: URL anchor name
    anchor_name=_("about"),
)
# Translators: section title
CONTACT = MenuItem(id='contact', url_name='core:contact', label=_("Contact"))
# Translators: section title
IMPRINT = MenuItem(id='imprint', url_name='core:imprint', label=_("Imprint"))
RECORDING = MenuItem(
    id=recording_category.id,
    url_name=f'recordings:{recording_category.id}',
    # Translators: section title
    label=_("Recording"),  # Hard coding necessary because user language is not yet accessible
)
REMASTERING = MenuItem(
    id=remastering_category.id,
    url_name=f'recordings:{remastering_category.id}',
    # Translators: section title
    label=_("Remastering"),  # Hard coding necessary because user language is not yet accessible
)
# Translators: section title
REFERENCES = MenuItem(
    id='references', url_name='references:references', label=_("References")
)


ALL_MENU_ITEMS = (ABOUT, CONTACT, IMPRINT, RECORDING, REMASTERING, REFERENCES)


HEADER_MENU_ITEMS = (RECORDING, REMASTERING, REFERENCES, ABOUT)


FOOTER_MENU_ITEMS__PRIMARY = (RECORDING, REMASTERING, REFERENCES)


FOOTER_MENU_ITEMS__SECONDARY = (ABOUT, CONTACT, IMPRINT)
