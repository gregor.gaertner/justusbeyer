from django.utils.translation import gettext as _

UI_LABELS = {
    # Translators: UI button label
    'show_all': _("show all")
}
