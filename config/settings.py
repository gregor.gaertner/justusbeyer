import environ
from easy_thumbnails.conf import Settings as thumbnail_settings

from django.utils.translation import gettext_lazy as _

env = environ.Env(
    ALLOWED_HOSTS=(list, []),
    CSRF_COOKIE_SECURE=(bool, False),
    DEBUG=(bool, False),
    EMAIL_HOST_PASSWORD=(str, None),
    SECURE_HSTS_INCLUDE_SUBDOMAINS=(bool, False),
    SECURE_HSTS_PRELOAD=(bool, False),
    SECURE_HSTS_SECONDS=(int, 0),
    SECURE_SSL_REDIRECT=(bool, False),
    SESSION_COOKIE_SECURE=(bool, False),
)
environ.Env.read_env()

root = environ.Path(__file__) - 2
public_root = root.path('public/')


# --------------------------------------------------------------------------------------------------
# Development

DEBUG = env('DEBUG')


# --------------------------------------------------------------------------------------------------
# Security

ALLOWED_HOSTS = env('ALLOWED_HOSTS')
CSRF_COOKIE_SECURE = env('CSRF_COOKIE_SECURE')
SECRET_KEY = env('SECRET_KEY')
SECURE_BROWSER_XSS_FILTER = True
SECURE_CONTENT_TYPE_NOSNIFF = True
SECURE_HSTS_INCLUDE_SUBDOMAINS = env('SECURE_HSTS_INCLUDE_SUBDOMAINS')
SECURE_HSTS_PRELOAD = env('SECURE_HSTS_PRELOAD')
SECURE_HSTS_SECONDS = env('SECURE_HSTS_SECONDS')
SECURE_SSL_REDIRECT = env('SECURE_SSL_REDIRECT')
SESSION_COOKIE_SECURE = env('SESSION_COOKIE_SECURE')
X_FRAME_OPTIONS = 'DENY'


# --------------------------------------------------------------------------------------------------
# Administration staff

ADMINS = [('Gregor Gärtner', 'django@gregorgaertner.de')]
MANAGERS = [('Gregor Gärtner', 'django@gregorgaertner.de')]


# --------------------------------------------------------------------------------------------------
# Databases

DATABASES = {'default': env.db()}
DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'


# --------------------------------------------------------------------------------------------------
# Application definition

AUTH_PASSWORD_VALIDATORS = [
    {'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator'},
    {'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator'},
    {'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator'},
    {'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator'},
]
INSTALLED_APPS = [
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django_cleanup',  # github.com/un1t/django-cleanup
    'apps.core.apps.CoreConfig',
    'apps.recordings.apps.RecordingsConfig',
    'apps.references.apps.ReferencesConfig',
    'modeltranslation',
    'django.contrib.admin',  # Admin must be named after app “core” in order that
    # apps.core.templates.admin.base_site.html will be used.
    'admin_ordering',  # github.com/matthiask/django-admin-ordering
    'easy_thumbnails',  # github.com/jonasundderwolf/django-image-cropping
    'image_cropping',  # github.com/jonasundderwolf/django-image-cropping
    'django_extensions',
]
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    # Consider the notes on middleware order regarding LocaleMiddleware in this section of the docs:
    # docs.djangoproject.com/en/2.2/topics/i18n/translation/#how-django-discovers-language-preference
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]
WSGI_APPLICATION = 'wsgi.application'


# --------------------------------------------------------------------------------------------------
# URL configuration

ROOT_URLCONF = 'config.urls'


# --------------------------------------------------------------------------------------------------
# Internationalization

USE_I18N = True


# --------------------------------------------------------------------------------------------------
# Localization

USE_L10N = True

LANGUAGE_CODE = 'en'
LANGUAGES = (('de', _("Deutsch")), ('en', _("English")))
ALL_LANGUAGE_CODES = [language[0] for language in LANGUAGES]  # For convenience
LOCALE_PATHS = (root.path('locale/'),)


# --------------------------------------------------------------------------------------------------
# Time zone

TIME_ZONE = 'Europe/Berlin'
USE_TZ = False


# --------------------------------------------------------------------------------------------------
# Static files

STATIC_ROOT = public_root('static')
STATIC_URL = '/static/'


# --------------------------------------------------------------------------------------------------
# User uploads

MEDIA_ROOT = public_root('media')
MEDIA_URL = '/media/'


# --------------------------------------------------------------------------------------------------
# Templates

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'apps.core.context_processors.site_map',
                'apps.core.context_processors.ui_labels',
            ]
        },
    }
]


# --------------------------------------------------------------------------------------------------
# Email

EMAIL_HOST = 'smtp.strato.de'
EMAIL_HOST_USER = 'django@gregorgaertner.de'
EMAIL_HOST_PASSWORD = env('EMAIL_HOST_PASSWORD')
EMAIL_PORT = 465
EMAIL_USE_SSL = True
SERVER_EMAIL = 'django@gregorgaertner.de'


# --------------------------------------------------------------------------------------------------
# Sessions

SESSION_COOKIE_AGE = 864000  # 10 days


# //////////////////////////////////////////////////////////////////////////////////////////////////
# CUSTOM
# //////////////////////////////////////////////////////////////////////////////////////////////////

# Place project-specific settings here.


# --------------------------------------------------------------------------------------------------
# django-image-cropping

THUMBNAIL_PROCESSORS = (
    'image_cropping.thumbnail_processors.crop_corners',
) + thumbnail_settings.THUMBNAIL_PROCESSORS
